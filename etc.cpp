#define HEATSYSTEM_H
#define HEATSYSTEM_H
// comment
// comment
enum
{
  eFanSpeed_0 = 0,
  eFanSpeed_1 = 1,
  eFanSpeed_2 = 2,
  eFanSpeed_3 = 3,
  eFanSpeed_4 = 4,
  eFanSpeed_Min = eFanSpeed_0,
  eFanSpeed_Max = eFanSpeed_4,
};

enum
{
  HW_TMP_LUX_OFF = 0,
  HW_TMP_LUX_3H = 1,
  HW_TMP_LUX_6H = 2,
  HW_TMP_LUX_12H = 3,
  HW_TMP_LUX_ONE_TIME_INCR = 4,
  HW_TMP_LUX_MIN = HW_TMP_LUX_OFF,
  HW_TMP_LUX_MAX = HW_TMP_LUX_ONE_TIME_INCR,
};

enum
{
  HW_ECONOMY = 0,
  HW_NORMAL = 1,
  HW_LUXURY = 2,
  HW_SMART = 4,
  HW_MIN = HW_ECONOMY,
  HW_MAX = HW_SMART,
};

enum
{
  OPMODE_AUTO = 0,
  OPMODE_MANUAL = 1,
  OPMODE_ONLYADD = 2,
  OPMODE_MIN = OPMODE_AUTO,
  OPMODE_MAX = OPMODE_ONLYADD,
};
